# Bitbucket Pipelines pipe: Pipelines clear cache

This pipe lets you clear all or selected Bitbucket Pipelines caches from your repository.

By default this will clear all dependency caches that exist for this repository, but it can also be configured to clear specific caches.
The pipe will warn you if no caches were found or if it fails to clear a specific cache.

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
script:
  - pipe: atlassian/bitbucket-clear-cache:3.1.1
    variables:
      BITBUCKET_USERNAME: "<string>"
      BITBUCKET_APP_PASSWORD: "<string>"
      # CACHES: "<array>"  # Optional
      # DEBUG: "<boolean>" # Optional
      # WORKSPACE: "<string>" # Optional
      # REPO_SLUG: "<string>" # Optional
```
## Variables

| Variable              | Usage                                                       |
| --------------------- | ----------------------------------------------------------- |
| BITBUCKET_USERNAME (*)    | Username of Bitbucket user that can retrieve the repository's caches.     |
| BITBUCKET_APP_PASSWORD (*)| Bitbucket app password for the user.                                      |
| CACHES                | An optional list of names of caches to clear. Default: `[]`. If empty, will clear all caches.  |
| WORKSPACE             | Workspace of repository to clear cache from. Default: current workspace.      |
| REPO_SLUG             | Repository to clear cache from. Default: current repo.      |

_(*) = required variable._

## Prerequisites

This pipe requires Bitbucket credentials (username and [app password](https://confluence.atlassian.com/bitbucket/app-passwords-828781300.html)) so that the pipe can communicate with Bitbucket APIs to retrieve and clear dependency caches.

When generating the app password, remember to check the `Pipelines Write` and `Repositories Read` permissions.


## Examples

Basic example. This pipe will clear all caches in the repository.

```yaml
script:
  - pipe: atlassian/bitbucket-clear-cache:3.1.1
    variables:
      BITBUCKET_USERNAME: $BITBUCKET_USERNAME
      BITBUCKET_APP_PASSWORD: $BITBUCKET_APP_PASSWORD
```

Advanced example. This pipe will clear the `maven` and `node` caches in the repository.

```yaml
script:
  - pipe: atlassian/bitbucket-clear-cache:3.1.1
    variables:
      BITBUCKET_USERNAME: $BITBUCKET_USERNAME
      BITBUCKET_APP_PASSWORD: $BITBUCKET_APP_PASSWORD
      CACHES: ["maven", "node"]
```

Advanced example. This pipe will clear caches in the given repository and workspace.

```yaml
script:
  - pipe: atlassian/bitbucket-clear-cache:3.1.1
    variables:
      BITBUCKET_USERNAME: $BITBUCKET_USERNAME
      BITBUCKET_APP_PASSWORD: $BITBUCKET_APP_PASSWORD
      WORKSPACE: "workspace"
      REPO_SLUG: "repo"
```

## Support
If you’d like help with this pipe, or you have an issue or feature request, [let us know on Community][community].

If you’re reporting an issue, please include:

- the version of the pipe
- relevant logs and error messages
- steps to reproduce


## License
Copyright (c) 2019 Atlassian and others.
Apache 2.0 licensed, see [LICENSE.txt](LICENSE.txt) file.


[community]: https://community.atlassian.com/t5/forums/postpage/board-id/bitbucket-pipelines-questions?add-tags=pipes,cache
